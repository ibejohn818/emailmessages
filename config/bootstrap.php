<?php

use Cake\Mailer\Email;
use Cake\Core\Configure;
Email::configTransport('email_messages_debug', [
    'className' => 'Debug',
]);


//comma seperated helpers to use in email views
define("EMAIL_MESSAGES_HELPERS","Html,Text,Time");
