<?php
use Cake\Routing\Router;

Router::plugin(
    'EmailMessages',
    ['path' => '/email-messages'],
    function ($routes) {
        $routes->fallbacks('DashedRoute');
    }
);

Router::prefix("admin",function($routes) {

    $routes->plugin("EmailMessages",['path'=>'/email-messages'],function($routes) {

        $routes->connect("/",[
            'plugin'=>'EmailMessages',
            'controller'=>'EmailMessages',
            'action'=>'index'
        ]);

        $routes->connect("/:controller",["action"=>"index"]);

        $routes->connect("/:controller/:action");

        $routes->fallBacks("DashedRoute");

    });

});
