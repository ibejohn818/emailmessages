<?php
namespace EmailMessages\Controller\Admin;

use EmailMessages\Controller\AppController;

/**
 * EmailMessages Controller
 *
 * @property \EmailMessages\Model\Table\EmailMessagesTable $EmailMessages
 */
class EmailMessagesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $emailMessages = $this->paginate($this->EmailMessages);

        $this->set(compact('emailMessages'));
        $this->set('_serialize', ['emailMessages']);
    }

    /**
     * View method
     *
     * @param string|null $id Email Message id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $emailMessage = $this->EmailMessages->get($id, [
            'contain' => []
        ]);

        $this->set('emailMessage', $emailMessage);
        $this->set('_serialize', ['emailMessage']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $emailMessage = $this->EmailMessages->newEntity();
        if ($this->request->is('post')) {
            $emailMessage = $this->EmailMessages->patchEntity($emailMessage, $this->request->data);
            if ($this->EmailMessages->save($emailMessage)) {
                $this->Flash->success(__('The email message has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The email message could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('emailMessage'));
        $this->set('_serialize', ['emailMessage']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Email Message id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $emailMessage = $this->EmailMessages->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $emailMessage = $this->EmailMessages->patchEntity($emailMessage, $this->request->data);
            if ($this->EmailMessages->save($emailMessage)) {
                $this->Flash->success(__('The email message has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The email message could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('emailMessage'));
        $this->set('_serialize', ['emailMessage']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Email Message id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $emailMessage = $this->EmailMessages->get($id);
        if ($this->EmailMessages->delete($emailMessage)) {
            $this->Flash->success(__('The email message has been deleted.'));
        } else {
            $this->Flash->error(__('The email message could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
