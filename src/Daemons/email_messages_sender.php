#!/usr/bin/php
<?php

/**
 * Absolute path to the cake shell for your app
 * Example: /var/www/mysite/bin/cake
 * @var string
 */
$shell = "/home/websites/CakeShare/cake2shell";

/**
 * The amount of emails to query per iteration
 * @var integer
 */
$send_limit = 20;

$pid = pcntl_fork();

if($pid === -1) {

    return 1;

} elseif($pid) {

    return 0;

} else {

    while(true) {

        $seconds = date("s");
        $minutes = date("i");
        $hours = date("H");
        $month = date("m");
        $day = date("d");
        $year = date("Y");
        $dow = date("D");

    	passthru("nohup {$shell} EmailMessages.sender run_sender >& /dev/null &");

        sleep(10);

    }

}
