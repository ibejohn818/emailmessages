<?php

namespace EmailMessages\Lib;

use EmailMessages\Model\Entity\EmailMessage;

/**
 * Interface that a referenced model must implement in order to create and send attachments
 * Should your email message reference a EmailMessage->model( Model\Table\EmailMessage->model ), the model will be checked to see if
 * it implements the IEmailMessage interface, if so, then the EmailMessage->model->beforeEmailSend()
 * & EmailMessage->model->afterEmailSend() methods will be executed before and after the email is sent
 */
interface IEmailMessage {

    /**
     * Executed directly before the email is sent
     * Should be used create attachments
     * Must return the EmailMessage entity that is given as the argument
     * @param  EmailMessage $email The EmailMessage entity in scope
     * @return EmailMessage             Return the EmailMessage entity that is given as argument
     */
    public function beforeEmailSend(EmailMessage $email);

    public function afterEmailSend(EmailMessage $email);

}
