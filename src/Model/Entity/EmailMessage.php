<?php
namespace EmailMessages\Model\Entity;

use Cake\ORM\Entity;

/**
 * EmailMessage Entity.
 *
 * @property int $id
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property string $to_name
 * @property string $to_email
 * @property string $subject
 * @property string $view_file
 * @property string $layout_file
 * @property string $model
 * @property string $foreign_key
 * @property string $reply_to
 * @property string $recipients
 * @property string $text_content
 * @property string $html_content
 * @property string $email_config
 * @property string $email_format
 * @property string $email_status
 * @property \Cake\I18n\Time $sent_datetime
 * @property string $serialized_data
 * @property string $email_to
 * @property string $email_cc
 * @property string $email_bcc

 */
class EmailMessage extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
        // 'email_to'=>false,
        // 'email_cc'=>false,
        // 'email_bcc'=>false
    ];

    //email_status
    const SENT      = "sent";
    const PENDING   = "pending";
    const SENDING   = "sending";
    const ERROR     = "error";


    public static function emailStatuses() {

        return [
            self::SENT      =>strtoupper(self::SENT),
            self::PENDING   =>strtoupper(self::PENDING),
            self::SENDING   =>strtoupper(self::SENDING),
            self::ERROR     =>strtoupper(self::ERROR),
        ];

    }

    public function addTo($email,$name = '') {

        if(!is_array($this->email_to)) {
            $this->email_to = [];
        }

        $this->email_to[$email]=$name;

    }

    public function addCc($email,$name = '') {

        if(!is_array($this->email_cc)) {
            $this->email_cc = [];
        }

        $this->email_cc[$email]=$name;

    }

    public function addBcc($email,$name = '') {

        if(!is_array($this->email_bcc)) {
            $this->email_bcc = [];
        }

        $this->email_bcc[$email]=$name;

    }

    public function addAttachment($filePath,$fileName = false, $mimeType = false, $contentID = false) {

        if(!isset($this->attachments) || !is_array($this->attachments)) {
            $this->attachments = [];
        }

        if(!$fileName && !$mimeType && !$contentID) {

            $this->attachments[] = $filePath;
            return;

        }

        $attrs = [];

        $pi = pathinfo($filePath);

        if(!$fileName) {
            $fileName = "{$pi['filename']}.{$pi['extension']}";
        }

        if($mimeType) {
            $attrs['mimetype'] = $mimeType;
        }

        if($contentID) {
            $attrs['contentId'] = $contentID;
        }

        $attrs['file'] = $filePath;

        $this->attachments[$fileName] = $attrs;


    }

}
