<?php
namespace EmailMessages\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use EmailMessages\Model\Entity\EmailMessage;
use EmailMessages\Lib\IEmailMessage;

use Cake\ORM\TableRegistry;
use Cake\Mailer\Email;
use Cake\Event\Event;
/**
 * EmailMessages Model
 *
 */
class EmailMessagesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('email_messages');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
    }

    public function implementedEvents() {
        return [
            'Model.beforeSave' => 'handleBeforeSave',
        ];
    }


    public function updateStatus($EmailMessageID,$status,$extra = []) {
        //get the email message entity
        $em = $this->get($EmailMessageID);

        if(!empty($extra)) {
            $em = $this->patchEntity($em,$extra);
        }

        $statuses = EmailMessage::emailStatuses();

        if(!array_key_exists($status,$statuses)) {
            throw new \Exception(__CLASS__."::".__METHOD__.":{$status} NOT A VALID CONSTANT IN EmailMessage ENTITY",500);
        }

        $em->email_status = $status;

        $res = $this->save($em);

        return $res;

    }

    public function handleBeforeSave(Event $event, EmailMessage $email) {

        if($email->isNew()) {
            //check to see if there is a to_email and to_name
            if(!empty($email->to_email)) {

                $email->addTo($email->to_email,$email->to_name);

            }

            if(!is_array($email->email_to)) {
                throw new \Exception("EmailMessage->email_to Must be an array of \$name=>\$email_address or {array_index}=>\$email_address. Use EmailMessage->addTo() method",500);
            } else {
                $email->email_to = serialize($email->email_to);
            }

            if(!empty($email->email_cc)) {

                if(!is_array($email->email_cc)) {
                    throw new \Exception("EmailMessage->email_cc Must be an array of \$name=>\$email_address or {array_index}=>\$email_address. Use EmailMessage->addCc() Method",500);
                } else {
                    $email->email_cc = serialize($email->email_cc);
                }

            }

            if(!empty($email->email_bcc)) {

                if(!is_array($email->email_bcc)) {
                    throw new \Exception("EmailMessage->email_bcc Must be an array of \$name=>\$email_address or {array_index}=>\$email_address. Use EmailMessage->addBcc() Method",500);
                } else {
                    $email->email_bcc = serialize($email->email_bcc);
                }

            }
        }



        return true;

    }

    public function addEmail(EmailMessage $email,$sendNow = false) {

        if(empty($email->layout_file)) {
            $email->layout_file = "EmailMessages.default";
        }

        if(empty($email->email_config)) {
            $email->email_config = "default";
        }

        if(empty($email->email_priority)) {
            $email->email_priority = 0;
        }

        //set status to pending

        $email->email_status = EmailMessage::PENDING;

        $result = $this->save($email);

        if($sendNow) {
            return $this->sendEmail($result);
        }

        return $result;

    }

    public function sendEmail(EmailMessage $EmailMessage,$debug = false) {

        if($EmailMessage->email_status != EmailMessage::PENDING) {
            return;
        }

        //update status to processing
        $this->updateStatus($EmailMessage->id,EmailMessage::SENDING);

        $email = new Email();

        $email->profile($EmailMessage->email_config);

        if($debug) {
            $email->transport("email_messages_debug");
        }

        //set the to
        $to = unserialize($EmailMessage->email_to);
        $toArray = [];
        foreach($to as $addr=>$name) {
            $email->addTo($addr,$name);
        }
        unset($to);


        //set the CC
        if(!empty($EmailMessage->email_cc)) {

            $cc = unserialize($EmailMessage->email_cc);
            $ccArray = [];
            foreach($cc as $addr=>$name) {
                $email->addCc($addr,$name);
            }
            unset($cc);

        }

        //set the BCC
        if(!empty($EmailMessage->email_bcc)) {

            $bcc = unserialize($EmailMessage->email_bcc);

            foreach($bcc as $addr=>$name) {
                $email->addBcc($addr,$name);
            }
            unset($bcc);
        }

        //view vars
        $viewVars = [
            'EmailMessage'=>$EmailMessage
        ];

        $email->viewVars($viewVars);

        //domain
        $email->domain($EmailMessage->email_domain);

        //set the helpers
        $helpers = explode(",",EMAIL_MESSAGES_HELPERS);
        $email->helpers = $helpers;

        //set the format if specified
        if(!empty($EmailMessage->email_format)) {
            $email->emailFormat($EmailMessage->email_format);
        }

        $email->subject($EmailMessage->subject);

        //set the templates
        $email->template($EmailMessage->view_file,$EmailMessage->layout_file);

        $model = '';

        if(!empty($EmailMessage->model)) {
            $model = TableRegistry::get($EmailMessage->model);
        }

        if($model instanceof IEmailMessage) {
            $EmailMessage = $model->beforeEmailSend($EmailMessage);
        }

        //check for attachments
        $this->processAttachments($EmailMessage,$email);

        //send the email
        if(!($res = $email->send())) {
            if(!$debug) {
                $msg = $this->updateStatus($EmailMessage->id,EmailMessage::ERROR);
            }

        } else {
            if(!$debug) {
                $msg = $this->updateStatus($EmailMessage->id,EmailMessage::SENT,['sent_datetime'=>new \DateTime()]);
            }
        }

        if($model instanceof IEmailMessage) {
            $EmailMessage = $model->afterEmailSend($EmailMessage);
        }

        if(!$debug) {
            return $msg;
        }


    }

    private function processAttachments(EmailMessage $msg, Email $email) {

        if(is_array($msg->attachments) && count($msg->attachments)) {

            $email->attachments($msg->attachments);

        }

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('to_name');

        $validator
            ->allowEmpty('to_email');

        $validator
            ->allowEmpty('subject');

        $validator
            ->allowEmpty('view_file');

        $validator
            ->allowEmpty('layout_file');

        $validator
            ->allowEmpty('model');

        $validator
            ->allowEmpty('foreign_key');

        $validator
            ->allowEmpty('reply_to');

        $validator
            ->allowEmpty('recipients');

        $validator
            ->allowEmpty('text_content');

        $validator
            ->allowEmpty('html_content');

        $validator
            ->allowEmpty('email_config');

        $validator
            ->allowEmpty('email_format');

        $validator
            ->allowEmpty('email_status');

        $validator
            ->dateTime('sent_datetime')
            ->allowEmpty('sent_datetime');

        return $validator;
    }
}
