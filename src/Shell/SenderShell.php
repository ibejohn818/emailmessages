<?php

namespace EmailMessages\Shell;

use Cake\ORM\TableRegistry;
use Cake\Console\Shell;
use EmailMessages\Model\Entity\EmailMessage;

class SenderShell extends Shell {

    private $lockFile = "/tmp/email-messages-sender-lock";

    public function runSender() {

        $limit = intval($this->params['limit']);

        if(!is_numeric($limit)) {
            $limit = 10;
        }

        if(file_exists($this->lockFile)) {

            $pid = file_get_contents($this->lockFile);

            $activePids = explode("\n",trim(`ps -e | awk '{print $1}'`));

            if(!in_array($pid,$activePids)) {
                unlink($this->lockFile);

            }
            $this->out("FILE LOCK");
            return false;

        }

        //create lock file

        $pid = getmypid();

        file_put_contents($this->lockFile, $pid);

        $EmailMessages = TableRegistry::get("EmailMessages.EmailMessages");


        $msgs = $EmailMessages->find();

        $msgs->select(['id'])
            ->where([
                'email_status'=>EmailMessage::PENDING
            ])
            ->limit($limit)
            ->order([
                'priority'=>'DESC'
            ]);

        $stats = [];

        foreach($msgs as $k=>$msg) {

            $res = $EmailMessages->sendEmail($msg);

            if(!isset($stats[$res->email_status])) {
                $stats[$res->email_status]=1;
            } else {
                $stats[$res->email_status]++;
            }

        }

        $this->out("Stats");

        $this->ht();

        foreach($stats as $k=>$v) {
            $this->out("{$k}:{$v}");
        }

    }

}
