<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Email Messages'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="emailMessages form large-9 medium-8 columns content">
    <?= $this->Form->create($emailMessage) ?>
    <fieldset>
        <legend><?= __('Add Email Message') ?></legend>
        <?php
            echo $this->Form->input('to_name');
            echo $this->Form->input('to_email');
            echo $this->Form->input('subject');
            echo $this->Form->input('view_file');
            echo $this->Form->input('layout_file');
            echo $this->Form->input('model');
            echo $this->Form->input('foreign_key');
            echo $this->Form->input('reply_to');
            echo $this->Form->input('recipients');
            echo $this->Form->input('text_content');
            echo $this->Form->input('html_content');
            echo $this->Form->input('email_config');
            echo $this->Form->input('email_format');
            echo $this->Form->input('email_status');
            echo $this->Form->input('sent_datetime', ['empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
