

<div class="container-fluid">
    <div class="page-header">
        <h1>
            Email Messages
        </h1>
    </div>
        <ul class="nav nav-pills">
            <li><?= $this->Html->link(__('New Email Message'), ['action' => 'add']) ?></li>
        </ul>
    <div class="index #admin-email-messages-index">
        <h3><?= __('Email Messages') ?></h3>
        <table cellpadding="0" cellspacing="0" class='table table-stripped table-hover table-bordered'>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('created') ?></th>
                    <th><?= $this->Paginator->sort('modified') ?></th>
                    <th><?= $this->Paginator->sort('to_name') ?></th>
                    <th><?= $this->Paginator->sort('to_email') ?></th>
                    <th><?= $this->Paginator->sort('subject') ?></th>
                    <th><?= $this->Paginator->sort('view_file') ?></th>
                    <th><?= $this->Paginator->sort('layout_file') ?></th>
                    <th><?= $this->Paginator->sort('model') ?></th>
                    <th><?= $this->Paginator->sort('foreign_key') ?></th>
                    <th><?= $this->Paginator->sort('reply_to') ?></th>
                    <th><?= $this->Paginator->sort('email_config') ?></th>
                    <th><?= $this->Paginator->sort('email_format') ?></th>
                    <th><?= $this->Paginator->sort('email_status') ?></th>
                    <th><?= $this->Paginator->sort('sent_datetime') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($emailMessages as $emailMessage): ?>
                <tr>
                    <td><?= $this->Number->format($emailMessage->id) ?></td>
                    <td><?= h($emailMessage->created) ?></td>
                    <td><?= h($emailMessage->modified) ?></td>
                    <td><?= h($emailMessage->to_name) ?></td>
                    <td><?= h($emailMessage->to_email) ?></td>
                    <td><?= h($emailMessage->subject) ?></td>
                    <td><?= h($emailMessage->view_file) ?></td>
                    <td><?= h($emailMessage->layout_file) ?></td>
                    <td><?= h($emailMessage->model) ?></td>
                    <td><?= h($emailMessage->foreign_key) ?></td>
                    <td><?= h($emailMessage->reply_to) ?></td>
                    <td><?= h($emailMessage->email_config) ?></td>
                    <td><?= h($emailMessage->email_format) ?></td>
                    <td><?= h($emailMessage->email_status) ?></td>
                    <td><?= h($emailMessage->sent_datetime) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $emailMessage->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $emailMessage->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $emailMessage->id], ['confirm' => __('Are you sure you want to delete # {0}?', $emailMessage->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>

</div>
