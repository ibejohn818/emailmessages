<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Email Message'), ['action' => 'edit', $emailMessage->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Email Message'), ['action' => 'delete', $emailMessage->id], ['confirm' => __('Are you sure you want to delete # {0}?', $emailMessage->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Email Messages'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Email Message'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="emailMessages view large-9 medium-8 columns content">
    <h3><?= h($emailMessage->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('To Name') ?></th>
            <td><?= h($emailMessage->to_name) ?></td>
        </tr>
        <tr>
            <th><?= __('To Email') ?></th>
            <td><?= h($emailMessage->to_email) ?></td>
        </tr>
        <tr>
            <th><?= __('Subject') ?></th>
            <td><?= h($emailMessage->subject) ?></td>
        </tr>
        <tr>
            <th><?= __('View File') ?></th>
            <td><?= h($emailMessage->view_file) ?></td>
        </tr>
        <tr>
            <th><?= __('Layout File') ?></th>
            <td><?= h($emailMessage->layout_file) ?></td>
        </tr>
        <tr>
            <th><?= __('Model') ?></th>
            <td><?= h($emailMessage->model) ?></td>
        </tr>
        <tr>
            <th><?= __('Foreign Key') ?></th>
            <td><?= h($emailMessage->foreign_key) ?></td>
        </tr>
        <tr>
            <th><?= __('Reply To') ?></th>
            <td><?= h($emailMessage->reply_to) ?></td>
        </tr>
        <tr>
            <th><?= __('Email Config') ?></th>
            <td><?= h($emailMessage->email_config) ?></td>
        </tr>
        <tr>
            <th><?= __('Email Format') ?></th>
            <td><?= h($emailMessage->email_format) ?></td>
        </tr>
        <tr>
            <th><?= __('Email Status') ?></th>
            <td><?= h($emailMessage->email_status) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($emailMessage->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($emailMessage->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($emailMessage->modified) ?></td>
        </tr>
        <tr>
            <th><?= __('Sent Datetime') ?></th>
            <td><?= h($emailMessage->sent_datetime) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Recipients') ?></h4>
        <?= $this->Text->autoParagraph(h($emailMessage->recipients)); ?>
    </div>
    <div class="row">
        <h4><?= __('Text Content') ?></h4>
        <?= $this->Text->autoParagraph(h($emailMessage->text_content)); ?>
    </div>
    <div class="row">
        <h4><?= __('Html Content') ?></h4>
        <?= $this->Text->autoParagraph(h($emailMessage->html_content)); ?>
    </div>
</div>
