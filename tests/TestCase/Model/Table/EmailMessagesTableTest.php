<?php
namespace EmailMessages\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use EmailMessages\Model\Table\EmailMessagesTable;

/**
 * EmailMessages\Model\Table\EmailMessagesTable Test Case
 */
class EmailMessagesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \EmailMessages\Model\Table\EmailMessagesTable
     */
    public $EmailMessages;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.email_messages.email_messages'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('EmailMessages') ? [] : ['className' => 'EmailMessages\Model\Table\EmailMessagesTable'];
        $this->EmailMessages = TableRegistry::get('EmailMessages', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->EmailMessages);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
